1. Set up a DevOps environment with observability
Set up a DevOps environment with observability (prometheus/grafana) locally using Docker and compose. The environment is made up of OneDev (used for version control as well as CI/CD) and a selfhosted docker registry used for delivery. All the documentation needed for the setup, as well as the relevant compose and Dockerfiles are to be provided in a public repository.

2: Custom OneDev prometheus exporter
A custom exporter for OneDev build data is to be written in Python using the official Prometheus client library. The repository should be public and OneDev CI/CD pipelines should be set up on the local instance for test and prod (delivery). Make sure to include your OneDev pipeline files in the public GitLab repo.

Delivery is done by means of a docker image to be pushed to the container registry via a pipeline. The exporter should be part of the environment in deliverable 1.

Remember to include documentation on how to set up Grafana to visualize data from your custom exporter.

Tasks: 
- Secure server (SSH, HTTPS), done
- Setup OneDev, done
- Setup Docker with Dockerfile
- Setup Grafana (Simon, Melina)
- Setup Prometheus (Saeed, Ahmed)


SETUP GRAFANA:
